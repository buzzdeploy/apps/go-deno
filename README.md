# Hello Deno

Building and running code from WASM.

This is just a simple example of compiling whatever language and running it with [Deno](https://deno.land/). The goal is to get a start using WASM compiled in other languages with a common framework concept. This is only used as "How can I get started?" and validating some current hype. 

## Use

In every folder, except the `deno` folder, is a `build.sh` script to build a wasm module and dump the WASM into the `deno` folder. 

Once all the folders have been built, use the `deno/run.sh` script to run the example.

## References
* [running a Go Program in Deno via WASM](https://dev.to/taterbase/running-a-go-program-in-deno-via-wasm-2l08)