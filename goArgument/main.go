package main

import (
	// "encoding/json"
	"fmt"
	"syscall/js"
)

func whoHello(input string) (string, error) {
	retval := fmt.Sprintf("Hello, %s!", input)
	return retval, nil
}

func jsonWrapper() js.Func {
	jsonFunc := js.FuncOf(func(this js.Value, args []js.Value) any {
		if len(args) != 1 {
			return "Invalid no of arguments passed"
		}
		inputJSON := args[0].String()
		// fmt.Printf("input %s\n", inputJSON)
		saywho, _ := whoHello(inputJSON)
		// if err != nil {
		// 	fmt.Printf("unable to convert to json %s\n", err)
		// 	return err.Error()
		// }
		return saywho
	})
	return jsonFunc
}

func main() {
	fmt.Println("Go Web Assembly Arguments!")
	// <-make(chan bool)
	c := make(chan struct{}, 0)
	js.Global().Set("whoHello", jsonWrapper())
	<-c // Allows function to finish, could use a wait group alternatively
}
