#!/bin/bash

GOOS=js GOARCH=wasm go build -o ../deno/whodeno.wasm
cp $(go env GOROOT)/misc/wasm/wasm_exec.js ../deno/wasm_exec.js

