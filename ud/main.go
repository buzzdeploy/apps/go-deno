package main

import (
	"fmt"
	api "wirewc.me/go/ud/api"
)

func main() {
	response, count, err := api.DefineMatch("Arch", "linux")
	if err != nil {
		panic(fmt.Errorf("decoding API response failed: %v", err))
	}

	defResponse :=
		fmt.Sprintf("Matches Found: %d\nDefinitions found:\n%s\n",
			count,
			response)

	fmt.Println(defResponse)
}
