package api

import (
	"fmt"
	"testing"
)

// Generic Smoke test of the Define function
func TestDefine(t *testing.T) {
	fmt.Println("\nTesting Define")
	response, err := Define("Ubuntu")
	if err != nil {
		t.Errorf("decoding API response failed: %v", err)
	}

	defResponse := fmt.Sprintf("%d definitions returned\n", len(response.Results))
	fmt.Println(defResponse)
}

// Reduction test
func TestDefineMatch(t *testing.T) {
	fmt.Println("Testing DefineMatch")

	fmt.Println("\nHappy Flow 1")

	// Happy flow good match
	goodmatch, goodmatchcount, goodmatcherr := DefineMatch("Ubuntu", "Microsoft")
	if goodmatcherr != nil {
		t.Errorf("Good Match happy flow failed")
	}

	if goodmatch == "" ||
		goodmatchcount <= 0 {
		t.Errorf("Happy flow match and count are unexpected")
	}

	happyflow1 := fmt.Sprintf("Matches Found: %d\nDefinitions found:\n%s\n", goodmatchcount, goodmatch)

	fmt.Println(happyflow1)

	fmt.Println("\nHappy flow 2, no matches")
	// Happy flow no matches
	nomatch, nomatchcount, nomatcherr := DefineMatch("Ubuntu", "taco")
	if nomatcherr != nil {
		t.Errorf("No Match happy flow failed")
	}

	if nomatch != "" ||
		nomatchcount != 0 {
		t.Errorf("Happy flow of nomatch and count are unexpected")
	}

	happyflow2 := fmt.Sprintf("Matches Found: %d\nDefinitions found:\n%s\n", nomatchcount, nomatch)

	fmt.Println(happyflow2)

}
